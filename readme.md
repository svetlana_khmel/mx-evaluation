
USED:
- Bootstrap 4:

https://reactstrap.github.io/

- React Date Picker
https://reactdatepicker.com/
https://www.npmjs.com/package/react-datepicker

- Redux-form
https://redux-form.com/7.4.2/examples/react-widgets/

- Redux-Saga

- Eslint, Prettier
https://medium.com/technical-credit/using-prettier-with-vs-code-and-create-react-app-67c2449b9d08

- react-d3-components
https://github.com/codesuki/react-d3-components
https://rumble-charts.github.io/

https://prettier.io/docs/en/ignore.html#ignoring-files

https://eslint.org/docs/user-guide/configuring.html#configuring-rules


```angular2html
 React JS Exercise - v18.0.0
 
In this exercise you need to create a React App that will have a form with the following fields:
• Company Symbol
• Start Date (YYYY-mm-dd)
• End Date (YYYY-mm-dd) • Email

When the user submits the form you must do the following:

1) Display on screen the historical quotes for the 
submitted company in the given date range in table format 
(Date, Open, High, Low, Close and Volume values). 

Rows must be sortable by Open, High, Low, Close and Volume values.

Company symbols can be found here:
http://www.nasdaq.com/screening/companies-by-name.aspx


Data should be retrieved by using the API:
https://www.quandl.com/api/v3/datasets/WIKI/{symbol}.json?order=asc&start_date={Y-m- d}&end_date={Y-m-d}


Examples
https://www.quandl.com/api/v3/datasets/WIKI/AAPL.json?order=asc&start_date=2003-01- 01&end_date=2003-03-06 
https://www.quandl.com/api/v3/datasets/WIKI/GOOG.json?order=asc&start_date=2017-01-01&end_date=2017-03-06
https://www.quandl.com/api/v3/datasets/WIKI/GOOG.json?order=asc&start_date=2018-02-03&end_date=2018-02-19

3) Display a chart of the open and close prices in the given date range (any charting library can be used, preferable d3.js)
4) Send an email using the submitted company’s name as subject (i.e. Google) and the start date and end date as body (i.e. "From 2016-01-01 to 2016-02-01").
       PHP Exercise - v18.0.0 1
 Notes
• Form fields must have validation (valid email, End Date must not be lower than Start Date)
• Date picker must be used for the date fields
• Bootstrap 4 CSS Framework
• Axios and/or Fetch API must be used for the API calls
• Prettier and eslint must be used for static analysis
• ES6 coding standard must be followed
• For server side (if needed) one of NodeJS, PHP, GO must be used
• Code must be covered with Unit tests using JEST and/or ENZYME
PHP Exercise - v18.0.0 2

```
