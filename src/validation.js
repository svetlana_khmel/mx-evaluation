export const required = value => (value ? undefined : 'Required');

export const dataPicker = (startValue, endValue) => {
  let startTimeDate = null;
  let endTimeDate = null;
  let message = {};

  if (startValue && endValue && typeof startValue === 'string') {
    startTimeDate = new Date(startValue.split('-'));
    endTimeDate = new Date(endValue.split('-'));

    if (startTimeDate.getTime() >= endTimeDate.getTime()) {
      message = {
        text: 'endDataShouldBeBigger',
        value: false,
      };
    } else {
      message = {
        text: '',
        value: true,
      };
    }
  }

  if (startValue === undefined || endValue === undefined) {
    message = {
      text: 'dateIsNotDefined',
      value: false,
    };
  }

  return message;
};
