const Actions = {
  GET_DATA: 'GET_DATA',
  LOAD_DATA: 'LOAD_DATA',
  SET_START_DATE: 'SET_START_DATE',
  SET_END_DATE: 'SET_END_DATE',
  FETCH_ERROR: 'FETCH_ERROR',
};

export default Actions;
