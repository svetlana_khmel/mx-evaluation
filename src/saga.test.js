import { select, put, call, all } from 'redux-saga/effects';
import { getData } from './actions/index';

import rootSaga, { fetchData, asyncFetch, actionWatcher } from './sagas';

describe('Fetch Data', () => {
  const generator = fetchData();

  it('Should get Select', () => {
    const result = generator.next().value;
    expect(result).toEqual(select());
  });

  it('Must call fetch ', () => {
    const testValue = generator.next().value;
    expect(testValue).toEqual(call(asyncFetch, undefined));
  });

  it('Must put getData', () => {
    const testValue = generator.next().value;
    expect(testValue).toEqual(put(getData()));
  });
});

describe('Root Saga ', () => {
  const generator = rootSaga();
  it('calls `all()` with the correct functions', () => {
    expect(generator.next().value).toEqual(all([actionWatcher()]));
  });
});
