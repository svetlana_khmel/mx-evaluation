import { select, put, call, takeLatest, all } from 'redux-saga/effects';
import fetch from 'node-fetch'; //  for tests
import actions from './constants';
import { getData, asyncFetchError } from './actions/index';

const baseUrl = 'https://www.quandl.com/api/v3/datasets/WIKI/';

const headers = {
  Accept: 'application.json',
  'Content-Type': 'application/json',
};

export function asyncFetch(state) {
  const companySymbol = state ? state.form.dataForm.values.companySymbol : '';
  const startDate = state.data.startDate ? state.data.startDate : '';
  const endDate = state.data.endDate ? state.data.endDate : '';

  return fetch(
    `${baseUrl}${companySymbol}.json?order=asc&start_date=${startDate}&end_date=${endDate}`,
    {
      method: 'GET',
      headers,
    },
  ).then(response => response.json());
}

export function* fetchData() {
  const state = yield select();
  try {
    const payload = yield call(asyncFetch, state);
    yield put(getData(payload));
  } catch (error) {
    yield put(asyncFetchError());
  }
}

export function* actionWatcher() {
  yield takeLatest(actions.LOAD_DATA, fetchData);
}

export default function* rootSaga() {
  yield all([actionWatcher()]);
}
