import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import renderer from 'react-test-renderer';
import App from './App';
import { retrivedData, data } from './testData/data';

import TableComponent from './components/Table';

const mockStore = configureMockStore();
const store = mockStore({});

it('App renders correctly', () => {
  const tree = renderer
    .create(
      <Provider store={store}>
        <App data={retrivedData} />
      </Provider>,
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('TableComponent renders correctly', () => {
  const tree = renderer.create(<TableComponent data={data} />).toJSON();
  expect(tree).toMatchSnapshot();
});
