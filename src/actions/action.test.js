import actions from '../constants';
import { loadData, getData, setStartDate, setEndDate } from './index';

describe('actions', () => {
  it('Should create an action to start load data to application', () => {
    const expectedAction = {
      type: actions.LOAD_DATA,
    };

    expect(loadData()).toEqual(expectedAction);
  });

  it('Should create an action to load data to application', () => {
    const payload = {
      dataset: { id: 1 },
    };

    const expectedAction = {
      type: actions.GET_DATA,
      payload,
    };

    expect(getData(payload)).toEqual(expectedAction);
  });

  it('Should create an action to fill start date', () => {
    const expectedAction = {
      type: actions.SET_START_DATE,
      payload: {
        startDate: '2018-02-01',
      },
    };
    const payload = { startDate: '2018-02-01' };

    expect(setStartDate(payload)).toEqual(expectedAction);
  });

  it('Should create an action to fill end date', () => {
    const expectedAction = {
      type: actions.SET_END_DATE,
      payload: {
        endDate: '2018-02-01',
      },
    };
    const payload = { endDate: '2018-02-01' };

    expect(setEndDate(payload)).toEqual(expectedAction);
  });
});
