import actions from '../constants';

export const loadData = () => ({
  type: actions.LOAD_DATA,
});

export const getData = payload => ({
  type: actions.GET_DATA,
  payload,
});

export const setStartDate = payload => ({
  type: actions.SET_START_DATE,
  payload,
});

export const setEndDate = payload => ({
  type: actions.SET_END_DATE,
  payload,
});

export const asyncFetchError = () => ({
  type: actions.FETCH_ERROR,
});
