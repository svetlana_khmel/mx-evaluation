import actions from './constants';

export default (state = {}, action) => {
  switch (action.type) {
    case actions.SET_START_DATE:
      return { ...state, ...action.payload };

    case actions.SET_END_DATE:
      return { ...state, ...action.payload };

    case actions.LOAD_DATA:
      return { ...state, loading: true };

    case actions.GET_DATA:
      return { ...state, ...action.payload, loading: false };

    default:
      return state;
  }
};
