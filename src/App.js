import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';
import DataForm from './containers/data-form';
import { loadData } from './actions';
import TableComponent from './components/Table';
import ChartContainer from './components/Chart';
import { dataPicker } from './validation';

class App extends Component {
  constructor() {
    super();
    this.getData = this.getData.bind(this);
  }

  getData() {
    const { startDate, endDate } = this.props;
    const validation = dataPicker(startDate, endDate);

    if (validation.value) {
      this.props.loadData();
    }
  }

  render() {
    const { dataset } = this.props.data || {};

    return (
      <Container>
        <Row>
          <Col>
            <DataForm onSubmit={this.getData} />
          </Col>
        </Row>

        {dataset && (
          <Row style={{ marginBottom: '30px' }}>
            <Col>
              <ChartContainer data={dataset.data} />
            </Col>
          </Row>
        )}
        {dataset && (
          <Row>
            <Col>
              <TableComponent data={dataset.data} />
            </Col>
          </Row>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  form: state.form,
  data: state.data,
  startDate: state.data ? state.data.startDate : {},
  endDate: state.data ? state.data.endDate : {},
});

const mapDispatchToProps = dispatch => ({
  loadData: () => {
    dispatch(loadData());
  },
});

App.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  loadData: PropTypes.func,
  data: PropTypes.shape(),
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
