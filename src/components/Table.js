import React, { Component } from 'react';
import uuidv1 from 'uuid/v1';
import { Table, Button } from 'reactstrap';
import FilledRow from '../containers/filled-row';

export default class TableComponent extends Component {
  constructor() {
    super();
    this.renderHeader = this.renderHeader.bind(this);
    this.state = { data: [] };
  }

  sortData(index) {
    this.setState({
      data: this.props.data.sort((a, b) => { // eslint-disable-line
        return a[index] < b[index];
      }),
    });
  }

  renderHeader() {
    return (
      <thead>
        <tr>
          <th>Date</th>
          <th>
            <Button color="info" onClick={() => this.sortData(1)}>
              Open
            </Button>
          </th>
          <th>
            <Button color="info" onClick={() => this.sortData(2)}>
              High
            </Button>
          </th>
          <th>
            <Button color="info" onClick={() => this.sortData(3)}>
              Low
            </Button>
          </th>
          <th>
            <Button color="info" onClick={() => this.sortData(4)}>
              Close
            </Button>
          </th>
          <th>
            <Button color="info" onClick={() => this.sortData(5)}>
              Volume
            </Button>
          </th>
        </tr>
      </thead>
    );
  }

  renderRow() {
    const data =
      this.state.data.length === 0 ? this.props.data : this.state.data;

    return data.map(value => <FilledRow key={uuidv1()} data={value} />);
  }

  render() {
    return (
      <Table>
        {this.renderHeader()}
        <tbody>{this.renderRow()}</tbody>
      </Table>
    );
  }
}
