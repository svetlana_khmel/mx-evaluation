import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BarChart } from 'react-d3-components';

class ChartContainer extends Component {
  constructor() {
    super();

    this.updateDimensions = this.updateDimensions.bind(this);
  }
  componentWillMount() {
    this.updateDimensions();
  }
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }
  updateDimensions() {
    this.setState({ width: window.innerWidth / 1.2 });
  }
  render() {
    const { data } = this.props;
    const dataToChat = data.reduce(
      (acc, curr) => {
        acc[0].values.push({ x: curr[0], y: curr[1] });
        acc[1].values.push({ x: curr[0], y: curr[4] });

        return acc;
      },
      [{ label: 'open', values: [] }, { label: 'close', values: [] }],
    );

    return (
      <BarChart
        groupedBars
        data={dataToChat}
        width={this.state.width}
        height={400}
        margin={{
          top: 10,
          bottom: 100,
          left: 30,
          right: 10,
        }}
        xAxis={{
          innerTickSize: 3,
          tickDirection: 'diagonal',
          tickPadding: 0,
          outerTickSize: 3,
        }}
      />
    );
  }
}

ChartContainer.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any),
};

export default ChartContainer;
