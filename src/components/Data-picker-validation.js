import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { dataPicker } from '../validation';

// Disabling line because I need class
// eslint-disable-next-line
class DataPickerValidation extends Component {
  render() {
    const { startDate, endDate } = this.props;
    let message = '';

    const validation = dataPicker(startDate, endDate);

    if (validation.text === 'dateIsNotDefined') {
      message = 'Please, choose the date.';
    }

    if (validation.text === 'endDataShouldBeBigger') {
      message = 'Please, choose bigger end date.';
    }

    return <div>{message}</div>;
  }
}

const mapStateToProps = ({ data }) => ({
  startDate: data ? data.startDate : {},
  endDate: data ? data.endDate : {},
});

DataPickerValidation.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
};


export default connect(mapStateToProps, null)(DataPickerValidation);

