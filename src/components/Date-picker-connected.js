import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { setStartDate, setEndDate } from '../actions/index';
import DatePickerComponent from './Date-picker-component';

class RenderDatePicker extends Component {
  constructor(props) {
    super(props);

    this.handleDispatch = this.handleDispatch.bind(this);
  }

  handleDispatch(date) {
    this.props.label === 'startDate' // eslint-disable-line
      ? this.props.setStartDate({ startDate: date })
      : this.props.setEndDate({ endDate: date });
  }

  render() {
    return (
      <DatePickerComponent
        label="startDate"
        handleDispatch={this.handleDispatch}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setStartDate: date => dispatch(setStartDate(date)),
  setEndDate: date => dispatch(setEndDate(date)),
});

RenderDatePicker.propTypes = {
  setStartDate: PropTypes.func,
  setEndDate: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(RenderDatePicker);
