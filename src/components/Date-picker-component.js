import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import PropTypes from 'prop-types';

import 'react-datepicker/dist/react-datepicker.css';

class DatePickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null, // moment()
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date,
    });

    const formatedDate = moment([
      date._d.getFullYear(), // eslint-disable-line
      date._d.getMonth(), // eslint-disable-line
      date._d.getDate(), // eslint-disable-line
    ])
      .month(1)
      .format('YYYY-MM-DD');

    this.props.handleDispatch(formatedDate);
  }

  render() {
    return (
      <DatePicker
        selected={this.state.startDate}
        onChange={this.handleChange}
        dateFormat="YYYY-MM-DD"
      />
    );
  }
}

DatePickerComponent.propTypes = {
  handleDispatch: PropTypes.func,
};


export default DatePickerComponent;

