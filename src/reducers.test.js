import reducer from './reducers';
import actions from './constants';

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({});
  });

  it('should handle GET_DATA', () => {
    const payload = { dataset: { id: 9775718, dataset_code: 'GOOG' } };
    const result = {
      loading: false,
      dataset: { id: 9775718, dataset_code: 'GOOG' },
    };

    expect(
      reducer(
        {},
        {
          type: actions.GET_DATA,
          payload,
        },
      ),
    ).toEqual(result);
  });

  it('should handle SET_START_DATE', () => {
    const payload = { startDate: '2018-02-04' };

    expect(
      reducer(
        {},
        {
          type: actions.SET_START_DATE,
          payload,
        },
      ),
    ).toEqual(payload);
  });

  it('should handle SET_END_DATE', () => {
    const payload = { endDate: '2018-02-04' };

    expect(
      reducer(
        {},
        {
          type: actions.SET_END_DATE,
          payload,
        },
      ),
    ).toEqual(payload);
  });

  it('should handle LOAD_DATA', () => {
    const payload = { loading: true };

    expect(
      reducer(
        {},
        {
          type: actions.LOAD_DATA,
          payload,
        },
      ),
    ).toEqual(payload);
  });
});
