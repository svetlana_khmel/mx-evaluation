import React from 'react';
import uuidv1 from 'uuid/v1';
import PropTypes from 'prop-types';

const FilledRow = ({ data }) => {
  const Cell = data
    ? data.map((value, index) => { // eslint-disable-line
      if (index <= 5) {
        return <td key={uuidv1()}>{value}</td>;
      }
    })
    : null;

  return <tr>{Cell}</tr>;
};

FilledRow.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any),
};

export default FilledRow;
