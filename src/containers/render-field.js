import React from 'react';
import { Label, Input } from 'reactstrap';

const renderField = ({
  input,
  label,
  htmlFor,
  type,
  meta: { touched, error, warning },
}) => (
  <div>
    <Label htmlFor={htmlFor}>{label}</Label>
    <div>
      <Input {...input} placeholder={label} type={type} />
      {touched &&
        ((error && <span className="error">{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

export default renderField;
