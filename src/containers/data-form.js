import React from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Button, Form, FormGroup, Label } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import renderField from './render-field';
import RenderDatePicker from '../components/Date-picker-connected';

import { required } from '../validation';
import DataPickerValidation from '../components/Data-picker-validation';

const DataFormContainer = ({ handleSubmit }) => (
  <div className="merchant-form">
    <Form onSubmit={handleSubmit}>
      <FormGroup>
        <Field
          name="companySymbol"
          htmlFor="companySymbol"
          label="Company Symbol"
          component={renderField}
          type="text"
          validate={[required]}
        />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="startDate">Start Date (YYYY-mm-dd)</Label>
        <RenderDatePicker label="startDate" />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="endDate">End Date (YYYY-mm-dd)</Label>
        <RenderDatePicker label="endDate" />
      </FormGroup>
      <DataPickerValidation />
      <Button color="success" type="submit">
        Submit
      </Button>
    </Form>
  </div>
);

const DataForm = reduxForm({
  form: 'dataForm',
})(DataFormContainer);

DataFormContainer.propTypes = {
  handleSubmit: PropTypes.func,
};

export default DataForm;
