import React from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { reducer as reduxFormReducer } from 'redux-form';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import data from './reducers';
import mySaga from './sagas';
import registerServiceWorker from './registerServiceWorker';

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  form: reduxFormReducer,
  data,
});

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(mySaga);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  // turned off eslint due to 'document' existence in render but not present in this file
  document.getElementById('root'), // eslint-disable-line
);

registerServiceWorker();
